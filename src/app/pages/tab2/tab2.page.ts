import { Component } from '@angular/core';
import { DataLocalService } from '../../services/data-local.service';
import { Storage } from '@ionic/storage-angular';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  constructor(
    public dataLocal: DataLocalService
  ) {}

  ngOnInit(){
    
  }

  abrirRegistro(registro){
    this.dataLocal.abrirRegistro(registro);
  }

  enviarCorreo(){
    this.dataLocal.enviarCorreo();
  }
}
